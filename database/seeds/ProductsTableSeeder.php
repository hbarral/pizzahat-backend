<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'name'=> 'Pepperoni',
                'description'=> 'Ingredients: Pepperoni, Fresh Mushrooms and Extra Cheese',
                'units'=> 500,
                'price'=> 8.27,
                'image'=> '/assets/images/pizza_01.jpg',
                'category'=> 'pizzas',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name'=> 'Roquefort',
                'description'=> 'Our Estelar pizza is balanced and is the favorite of many of our customers, it is not the typical chicken and mushroom pizza, the touch of the bacon is perfect and makes it special.',
                'units'=> 500,
                'price'=> 4.66,
                'image'=> '/assets/images/pizza_02.jpg',
                'category' => 'pizzas',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name'=> 'Muzzarella',
                'description'=> 'A very special, tasty and light pizza: onion and roquefort cheese pizza, with barely and black olives, and a touch of arugula.',
                'units'=> 500,
                'price'=> 10.27,
                'image'=> '/assets/images/pizza_03.jpg',
                'category'=> 'pizzas',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name'=> 'Hawaiana',
                'description'=> 'The pizza that some question but all love. Ham, pineapple. Ingredients u003d> Ham and Pineapple.',
                'units'=> 500,
                'price'=> 10.67,
                'image'=> '/assets/images/pizza_04.jpg',
                'category'=> 'pizzas',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name'=> 'Veggie',
                'description'=> 'Only vegetables! Our meatless option. Bell pepper, mushrooms, olives, onion. Ingredients: Pepper, Onion, Fresh Mushrooms and Olives.',
                'units'=> 500,
                'price'=> 13.35,
                'image'=> '/assets/images/pizza_05.jpg',
                'category'=> 'pizzas',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name'=> 'Cuatro Quesos',
                'description'=> 'The pizza for cheese lovers. Mozzarella cheese, cream cheese, cheddar cheese, Parmesan cheese.',
                'units'=> 500,
                'price'=> 5.33,
                'image'=> '/assets/images/pizza_06.jpg',
                'category'=> 'pizzas',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name'=> 'Extravaganzza',
                'description'=> 'The most inclusive pizza of our specialties. Pepperoni, ground beef, ham, chorizo, pepper, onion, mushrooms, olives, extra cheese.',
                'units'=> 500,
                'price'=> 6.53,
                'image'=> '/assets/images/pizza_07.jpg',
                'category'=> 'pizzas',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name'=> 'Deluxe',
                'description'=> 'Enjoying this pizza is a luxury. Pepperoni, ground beef, mushrooms, bell pepper, onion.',
                'units'=> 500,
                'price'=> 9.47,
                'image'=> '/assets/images/pizza_08.jpg',
                'category'=> 'pizzas',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name'=> 'Pepsi',
                'description'=> 'Pepsi 1.5 L',
                'units' => 100,
                'price'=> 1.86,
                'image'=> '/assets/images/pepsi_01.jpg',
                'category'=> 'drinks',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name'=> 'Pepsi 600ml',
                'description'=> 'Pepsi 600ml',
                'units'=> 100,
                'price'=> 0.83,
                'image'=> '/assets/images/pepsi_02.jpg',
                'category'=> 'drinks',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name'=> 'Coca-Cola',
                'description'=> 'Coca-Cola 1.5 L',
                'units' => 100,
                'price'=> 1.95,
                'image'=> '/assets/images/cocacola_01.jpg',
                'category'=> 'drinks',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name'=> 'Coca-Cola 600ml',
                'description'=> 'Coca-Cola 600ml',
                'units'=> 100,
                'price'=> 0.97,
                'image'=> '/assets/images/cocacola_02.jpg',
                'category'=> 'drinks',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name'=> 'Delivery 5Km',
                'description'=> 'Delivery service, 5km around',
                'units'=> 1,
                'price'=> 2.65,
                'image'=> '/assets/images/delivery.png',
                'category'=> 'delivery',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name'=> 'Delivery Free Promo 30$',
                'description'=> 'Free delivery service for orders of $ 30 or more.',
                'units'=> 1,
                'price'=> 0,
                'image'=> '/assets/images/delivery_free.png',
                'category'=> 'delivery',
                'created_at' => new DateTime,
                'updated_at' => null
            ],
        ];

        DB::table('products')->insert($products);
    }
}
