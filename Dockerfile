# Dockerfile
FROM php:8.0-cli

RUN apt update -y && apt install -y \
    curl \
    libmcrypt-dev \
    libonig-dev \
    libpng-dev \
    libxml2-dev \
    libzip-dev \
    unzip \
    zip \
    zlib1g-dev

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN docker-php-ext-install \
      pdo \
      mbstring \
      pdo_mysql \
      mysqli \
      zip \
      && docker-php-ext-configure intl \
      && docker-php-ext-install intl \
      && docker-php-ext-configure gd \
      && docker-php-ext-install -j$(nproc) gd \
      && docker-php-source delete

WORKDIR /app

COPY . /app

RUN composer install

EXPOSE 8000

CMD php artisan serve --host=0.0.0.0 --port=8000
