<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UserController@login');

Route::get('products/{product}', 'ProductController@index');

Route::resource('/orders', 'OrderController');

Route::post('/purchases', 'PurchasesController@store');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('me', 'UserController@me');
    Route::post('logout', 'UserController@logout');
});
