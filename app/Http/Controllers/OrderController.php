<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderProduct;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Stripe\Charge;
use Stripe\Stripe;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Order::with(['products'])->orderBy('created_at', 'desc')->get(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payload = json_decode($request->getContent(), true);

        $rules = [
            'products' => 'required|array',
            'products.*.id' => 'required|integer',
            'products.*.quantity' => 'required|integer',
            'address' => 'required|string',
            'orderIntentToken' => 'required'
        ];

        $validator = Validator::make($payload, $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), 200);
        }

        try {
            $products = $payload['products'];
            $subtotal = $this->getTotal($payload);
            $delivery = ($subtotal >= 30) ? Product::where('name', 'Delivery Free Promo 30$')->first() : Product::where('name', 'Delivery 5Km')->first();


            $order = Order::create([
                'user_id' => null,
                'total' => $subtotal,
                'currency' => 'us_dollar', // for now, only in dollars.
                'symbol' => '$', // for now, only in dollars.
                'address' => $payload['address'],
                'status' => 'pending'
            ]);

            foreach ($products as $product) {
                OrderProduct::create([
                    'order_id' => $order->id,
                    'product_id' => $product['id'],
                    'quantity' => $product['quantity']
                ]);
            }

            OrderProduct::create([
                'order_id' => $order->id,
                'product_id' => $delivery->id,
                'quantity' => 1
            ]);

            $order->total = $order->total + $delivery->price;

            try {
                Stripe::setApiKey(env('STRIPE_SECRET'));
                Charge::create([
                    'amount' => $order->total * 100,
                    'currency' => 'usd',
                    'source' => $payload['orderIntentToken'],
                    'description' => 'Order #' . $order->id
                ]);
            } catch (\Exception $e) {
                return response()->json(['status' => false, 'message' => $e->getMessage()], 200);
            }

            $order->save();

            return response()->json([
                'status' => (bool) $order,
                'data' => $order,
                'message' => $order ? 'Order created!' : 'Error creating order'
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::with('products')->find($id);
        return response()->json($order, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $status = $order->update(
            $request->only(['quantity'])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Order updated!' : 'Error updating order'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $status = $order->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Order deleted!' : 'Error deleting order'
        ]);
    }

    public function deliverOrder(Order $order)
    {
        $order->is_delivered = true;
        $status = $order->save();

        return response()->json([
            'status' => $status,
            'data' => $order,
            'message' => $status ? 'Order delived!' : 'Error delivering order'
        ]);
    }

    private function getTotal($payload)
    {
        $total = [];
        $requiredProducts = $payload['products'];
        $ids = [];

        foreach ($requiredProducts as $product) {
            $ids[] = $product['id'];
        }

        $products = DB::table('products')
        ->whereIn('id', $ids)
        ->get();

        foreach ($products as $key => $product) {
            $price = $product->price;
            $quantity = $requiredProducts[$key]['quantity'];
            $subTotal = $price * $quantity;

            $total[] = $subTotal;
        }

        return array_sum($total);
    }
}
